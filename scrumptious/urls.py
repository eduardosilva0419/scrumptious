"""scrumptious URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.shortcuts import redirect


def redirect_to_recipe_list(request):
    return redirect("recipe_list")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("recipes/", include("recipes.urls")),
    path("", redirect_to_recipe_list, name="home_page"),
    path("accounts/", include("accounts.urls")),
]
# path 2: says whenever we navigate to "" (empty path, which is everytime we run the server), we will go inside the URL patterns of the recipes app and run the show_recipes function
# path 1: says whenever we navigate to "admin/" we will go inside the URL patterns of the admin app and run the admin.site.urls function
# adding paths in this file is what defines the URLs for the website. the description of the URLs is in recipes/urls.py which means we are going to go inside the recipes app and run the show_recipes function.
#  so if we create anything here, it will define the URLs for the website.
